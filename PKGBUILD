# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Stefano Capitani <stefanoatmanjarodotorg>
# Contributor: Tom Wadley <tom@tomwadley.net>

pkgname=nvm
pkgver=0.40.2
pkgrel=1
pkgdesc="Node Version Manager - Simple bash script to manage multiple active node.js versions"
arch=('any')
url="https://github.com/nvm-sh/nvm"
license=('MIT')
makedepends=('git')
install="$pkgname.install"
source=("git+https://github.com/nvm-sh/nvm.git#tag=v${pkgver}?signed"
        "init-$pkgname.sh"
        "install-$pkgname-exec")
sha256sums=('444ec6cfd249417159f60744713bda4fb931d74bd23333b7435cf5cc30cd0d9a'
            '692317bfd036557f59543fef9b67ff38de68208d30361fe385291f58d3ac0425'
            '795d3f6ad3076aa4b0bb9cc48a2e6e79331d121278a887667fb707181a54a10b')
validpgpkeys=('951E2402099DDEBA3E0227AF9F6A681E35EF8B56') # Jordan Harband <ljharb@gmail.com>

package() {
  cd "$pkgname"

  # nvm itself
  install -Dm644 "$pkgname.sh" -t "$pkgdir/usr/share/$pkgname/"

  # nvm-exec script for 'nvm exec' command
  install -Dm755 "$pkgname-exec" -t "$pkgdir/usr/share/$pkgname/"

  # bash completion
  install -Dm644 bash_completion -t "$pkgdir/usr/share/$pkgname/"

  # license
  install -Dm644 LICENSE.md -t "$pkgdir/usr/share/licenses/$pkgname/"

  # convenience script
  install -Dm644 "$srcdir/init-$pkgname.sh" -t "$pkgdir/usr/share/$pkgname/"

  # companion script which installs symlinks in NVM_DIR (see comment in script)
  install -Dm644 "$srcdir/install-$pkgname-exec" -t "$pkgdir/usr/share/$pkgname/"
}
